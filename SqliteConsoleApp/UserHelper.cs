﻿using System;

namespace SqliteConsoleApp
{
    public static class UserHelper
    {
        public static void Manual()
        {
            while (true)
            {
                Console.WriteLine("enter i for insert, s for select,c for count, u for update, d for delete, q for exit");

                var input = Console.ReadLine();

                switch (input)
                {
                    case "i":
                        PersonManager.CreateDb();
                        PersonManager.Insert();
                        break;

                    case "s":
                        PersonManager.Select();
                        break;

                    case "c":
                        PersonManager.Count();
                        break;

                    case "u":
                        PersonManager.Update();
                        break;

                    case "d":
                        PersonManager.Delete();
                        break;

                    case "q":
                        Environment.Exit(-1);
                        break;

                    default:
                        Console.WriteLine("please choose an action");
                        break;
                }
            }
        }
    }
}
