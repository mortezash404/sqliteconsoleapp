﻿using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace SqliteConsoleApp
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public AppDbContext()
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=App.db");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");
            modelBuilder.Entity<Person>().Property(p => p.Name).HasMaxLength(20);
            modelBuilder.Entity<Person>().Property(p => p.Phone).HasMaxLength(16);
            base.OnModelCreating(modelBuilder);
        }
    }
}
