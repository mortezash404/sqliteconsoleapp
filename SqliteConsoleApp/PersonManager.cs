﻿using System;
using System.Linq;

namespace SqliteConsoleApp
{
    public static class PersonManager
    {
        public static void CreateDb()
        {
            var context = new AppDbContext();

            context.Database.EnsureCreated();
        }

        public static void Select()
        {
            using var context = new AppDbContext();

            var persons = context.Persons.ToList();

            foreach (var person in persons)
            {
                Console.WriteLine($" Id is : {person.Id}, Name is : {person.Name}, Phone is : {person.Phone}");
            }

            Console.WriteLine(" - - - - - - - - - - - - - - - - - - - - \n");
        }

        public static void Count()
        {
            using var context = new AppDbContext();

            Console.WriteLine(context.Persons.Count());
        }

        public static void Insert()
        {
            Console.WriteLine("insert Name: ");

            var name = Console.ReadLine();

            Console.WriteLine("insert Phone: ");

            var phone = Console.ReadLine();

            using (var context = new AppDbContext())
            {
                context.Persons.Add(new Person
                {
                    Name = name,
                    Phone = phone
                });

                context.SaveChanges();
            }

            Console.WriteLine($"{name} saved.");
        }

        public static void Update()
        {
            Console.WriteLine("insert Id: ");

            var id = int.Parse(Console.ReadLine() ?? string.Empty);

            Console.WriteLine("insert Phone: ");

            var phone = Console.ReadLine();

            using var context = new AppDbContext();

            var person = context.Persons.Find(id);

            if (person == null)
            {
                Console.WriteLine("Enter valid Id.\n");
            }
            else
            {
                person.Phone = phone;

                context.SaveChanges();

                Console.WriteLine("person updated.\n");
            }
        }

        public static void Delete()
        {
            Console.WriteLine("insert Id: ");

            var id = int.Parse(Console.ReadLine() ?? string.Empty);

            using var context = new AppDbContext();

            var person = context.Persons.Find(id);

            if (person == null)
            {
                Console.WriteLine("Enter valid Id.\n");
            }
            else
            {
                context.Remove(person);

                context.SaveChanges();

                Console.WriteLine("person removed.\n");
            }
        }
    }
}
